import App from './app/reducer';
import Todos from './todos/reducer';
import LanguageSwitcher from './languageSwitcher/reducer';

export default {
  App,
  LanguageSwitcher,
  Todos,
};
