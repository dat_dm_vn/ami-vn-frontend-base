import actions, { getView } from "./actions";


const initState = {
  view: getView(window.innerWidth),
  height: window.innerHeight,
};

export default function appReducer(state = initState, action) {
  switch (action.type) {
    case actions.CHANGE:
      return {
        ...state,
      };
    default:
      return state;
  }
}
