import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
import LanguageSwitcher from '../LanguageSwitcher';
import TopbarWrapper from "./topbar.style";
import logo from "../../image/logo.png"

const { Header } = Layout;

class Topbar extends Component {
    render() {
        const styling = {
            position: "fixed",
            width: "100%",
            height: 70
        };
        return (
            <TopbarWrapper>
                <Header
                    style={styling}
                    className={"trueTopbar"}
                >
                    <img className="trueLogo" src={logo} alt="logo" />
                    <ul className="trueRight">
                        <li><LanguageSwitcher /></li>
                    </ul>
                </Header>
            </TopbarWrapper>
        );
    }
}

export default connect(
    state => ({
        ...state.App,
        locale: state.LanguageSwitcher.language.locale,
    }),
    {}
)(Topbar);
