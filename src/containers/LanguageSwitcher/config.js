import { language } from '../../settings';

import englishLang from '../../image/flag/uk.svg';
import vnLang from '../../image/flag/vn.svg';

const config = {
    defaultLanguage: language,
    options: [
        {
            languageId: 'english',
            locale: 'en',
            text: 'English',
            icon: englishLang,
        },
        {
            languageId: 'vietnamese',
            locale: 'vi',
            text: 'Vietnamese',
            icon: vnLang,
        },
    ],
};

export function getCurrentLanguage(lang) {
    let selecetedLanguage = config.options[0];
    config.options.forEach(language => {
        if (language.languageId === lang) {
            selecetedLanguage = language;
        }
    });
    return selecetedLanguage;
}
export default config;
