import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, LocaleProvider } from 'antd';
import { IntlProvider } from 'react-intl';
import Topbar from '../Topbar/topbar';
import Todo from '../Todo';
import { siteConfig } from '../../settings';
import { AppLocale } from '../../trueapp/true.app';
import AppStyled from './app.style';
import './global.css';

const { Content, Footer } = Layout;
export class App extends Component {
    render() {
        const { url } = this.props.match;
        const { locale, height } = this.props;
        const currentAppLocale = AppLocale[locale];
        const appHeight = window.innerHeight;
        return (
            <LocaleProvider locale={currentAppLocale.antd}>
                <IntlProvider
                    locale={currentAppLocale.locale}
                    messages={currentAppLocale.messages}
                >
                    <AppStyled>
                        <Layout style={{ height: appHeight }}>
                            <Topbar url={url} />
                            <Layout style={{ flexDirection: 'row', overflowX: 'hidden' }}>
                                <Layout
                                    className="trueContentMainLayout"
                                    style={{
                                        height: height
                                    }}
                                >
                                    <Content
                                        className="trueContent"
                                        style={{
                                            padding: '70px 0 0',
                                            flexShrink: '0',
                                            background: '#f1f3f6',
                                            position: 'relative'
                                        }}
                                    >
                                        <Todo />
                                    </Content>
                                    <Footer
                                        style={{
                                            background: '#ffffff',
                                            textAlign: 'center',
                                            borderTop: '1px solid #ededed'
                                        }}
                                    >
                                        {siteConfig.footerText}
                                    </Footer>
                                </Layout>
                            </Layout>
                        </Layout>
                    </AppStyled>
                </IntlProvider>
            </LocaleProvider>
        );
    }
}

export default connect(
    state => ({
        locale: state.LanguageSwitcher.language.locale,
        height: state.App.height
    }),
    {}
)(App);
