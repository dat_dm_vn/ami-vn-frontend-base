import React from 'react';
import { Provider } from 'react-redux';
import { store, history } from '../redux/store';
import PublicRoutes from '../routes/router';
import { ThemeProvider } from 'styled-components';
import { LocaleProvider } from 'antd';
import { IntlProvider } from 'react-intl';
import themes from '../settings/themes';
import AppLocale from '../languageProvider';
import config, {
    getCurrentLanguage,
} from '../containers/LanguageSwitcher/config';
import { themeConfig } from '../settings';
import TrueAppStyled from './true.app.style';
import GlobalStyles from '../static/style/globalStyle';

const currentAppLocale =
    AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale];

const TrueApp = () => (
    <LocaleProvider locale={currentAppLocale.antd}>
        <IntlProvider
            locale={currentAppLocale.locale}
            messages={currentAppLocale.messages}
        >
            <ThemeProvider theme={themes[themeConfig.theme]}>
                <TrueAppStyled>
                    <Provider store={store}>
                        <PublicRoutes history={history} />
                    </Provider>
                    <GlobalStyles />
                </TrueAppStyled>
            </ThemeProvider>
        </IntlProvider>
    </LocaleProvider>
);

export default TrueApp;
export { AppLocale };
