export default {
    apiUrl: 'http://truemoney.com/api/'
};
const siteConfig = {
    siteName: 'trueapp',
    siteIcon: 'ion-flash',
    footerText: 'trueapp ©2019 Created by TrueMoney VietNam'
};

const themeConfig = {
    topbar: 'themedefault',
    sidebar: 'themedefault',
    layout: 'themedefault',
    theme: 'themedefault'
};
const language = 'english';

export {
    siteConfig,
    themeConfig,
    language,
  };
  