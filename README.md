## Quick Overview

```sh
npm install
npm run start
```

_([npm](https://medium.com/@maybekatz/introducing-npx-an-npm-package-runner-55f7d4bd282b) comes with npm 8.16.0 and higher, see [instructions for older npm versions](https://gist.github.com/gaearon/4064d3c23a77c74a3614c498a8bb1c5f))_

Then open [http://localhost:3000/](http://localhost:3000/) to see your app.<br>
When you’re ready to deploy to production, create a minified bundle with `npm run build`.
```sh
npm run build
```

## Project Structure:
```bash
├── public
|  ├── css
|  ├── fonts
|  └── iconfont
└── src
|  ├── components
|  ├── containers
|  ├── helpers
|  ├── image
|  ├── languageProvider
|  ├── redux
|  ├── routes
|  ├── settings
|  ├── static
|  └── trueapp
```
## What’s Included?

Your environment will have everything you need to build a modern single-page React app:

- React, JSX, ES6, TypeScript and Flow syntax support.
- State manament with Redux-Saga
- UI Library with Ant Design
- Localization with React-Intl
- Routing with React-Routers
- Language extras beyond ES6 like the object spread operator.
- Autoprefixed CSS, so you don’t need `-webkit-` or other prefixes.
- A fast interactive unit test runner with built-in support for coverage reporting.
- A live development server that warns about common mistakes.
- A build script to bundle JS, CSS, and images for production, with hashes and sourcemaps.
- An offline-first [service worker](https://developers.google.com/web/fundamentals/getting-started/primers/service-workers) and a [web app manifest](https://developers.google.com/web/fundamentals/engage-and-retain/web-app-manifest/), meeting all the [Progressive Web App](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app) criteria. (_Note: Using the service worker is opt-in as of `react-scripts@2.0.0` and higher_)
- Hassle-free updates for the above tools with a single dependency.

## Acknowledgements

We are grateful to the authors of existing related projects for their ideas and collaboration:

- [@datdm](https://bitbucket.org/dat_dm_vn)

## License

[@TrueApp](https://truemoney.com.vn) ©2019 Created by TrueMoney VietNam